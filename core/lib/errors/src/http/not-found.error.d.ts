import { HttpError } from "./error";
export declare class NotFoundError extends HttpError {
    constructor(message: string);
}
