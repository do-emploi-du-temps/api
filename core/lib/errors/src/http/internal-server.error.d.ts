import { HttpError } from "./error";
export declare class InternalServerError extends HttpError {
    constructor(message: string);
}
