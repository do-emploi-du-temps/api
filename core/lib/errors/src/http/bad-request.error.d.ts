import { HttpError } from "./error";
export declare class BadRequestError extends HttpError {
    constructor(message: string);
}
