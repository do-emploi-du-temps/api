export declare class HttpError {
    protected httpCode: number;
    protected httpCodeText: string;
    protected message: string;
    constructor(httpCode: number, httpCodeText: string, message: string);
}
