"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.HttpError = void 0;
var HttpError = /** @class */ (function () {
    function HttpError(httpCode, httpCodeText, message) {
        this.httpCode = httpCode;
        this.httpCodeText = httpCodeText;
        this.message = message;
    }
    return HttpError;
}());
exports.HttpError = HttpError;
