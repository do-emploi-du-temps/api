export interface HttpResponse {
    status: number;
    response: any;
}
