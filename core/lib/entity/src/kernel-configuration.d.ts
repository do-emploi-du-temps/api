/// <reference types="node" />
import { Server } from "http";
/**
 * Data structure for Kernel configuration.
 */
export interface KernelConfiguration {
    modules: any[];
    middlewares: any[];
    healthModule?: boolean;
    routePrefix?: string;
    debug?: boolean;
    onGracefulShutdown?: (server: Server) => void;
}
