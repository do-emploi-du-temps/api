"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Kernel = void 0;
var dotenv = __importStar(require("dotenv"));
var express_1 = __importDefault(require("express"));
var environment_1 = require("../constants/src/environment");
var decorators_1 = require("../decorators");
var modules_1 = require("../modules");
var utils_1 = require("../utils");
// Load the environment from current environment file
dotenv.config({ path: environment_1.ENV_FILE });
/**
 * @class Kernel
 * @description This class represent the core of the framework. It is the super-class of your application.
 * @author Thomas Gouveia
 */
var Kernel = /** @class */ (function () {
    /**
     * @method constructor
     * @description Create an instance of the Kernel
     * @param config :
     */
    function Kernel(config) {
        /**
         * @var DEFAULT_PORT
         * @description the default port on which the app listen
         */
        this.DEFAULT_PORT = 4000;
        var isHealth = typeof config.healthModule === "boolean" ? config.healthModule : true;
        this._app = express_1.default();
        this._prefix = config.routePrefix;
        this._modules = isHealth ? __spreadArrays(config.modules, [modules_1.HealthModule]) : config.modules;
        this._middlewares = config.middlewares;
        this._gracefulShutdownHandler = config.onGracefulShutdown;
        // Launch application middlewares registration
        decorators_1.attachMiddlewares(this._app, this._middlewares);
        // Launch modules registration
        decorators_1.attachModules(this._app, this._modules, this._prefix);
        if (config.debug)
            utils_1.debug(this._modules, this._middlewares, this._prefix);
    }
    /**
     * @method getPort()
     * @description return the port on which the application will listen
     * @returns the port
     */
    Kernel.prototype._getPort = function () {
        var PORT = process.env.PORT;
        return PORT ? parseInt(PORT, 10) : this.DEFAULT_PORT;
    };
    Kernel.prototype._handleShutdownGracefully = function (server) {
        if (this._gracefulShutdownHandler)
            this._gracefulShutdownHandler(server);
        else {
            console.info("\nGracefully shutdown application.");
            server.close(function () {
                console.info("Server closed.");
                process.exit(0);
            });
        }
    };
    /**
     * @method run()
     * @description run the application on the specified port, or by default on port 3000
     * @param port the port on which the application will listen
     */
    Kernel.prototype.run = function () {
        var _this = this;
        var port = this._getPort();
        var server = this._app.listen(port, function () {
            return utils_1.Logger.success("App is running on " + ("http://localhost:" + port + (_this._prefix || "")).yellow);
        });
        process.on("SIGINT", function () { return _this._handleShutdownGracefully(server); });
        process.on("SIGTERM", function () { return _this._handleShutdownGracefully(server); });
        process.on("SIGHUP", function () { return _this._handleShutdownGracefully(server); });
    };
    return Kernel;
}());
exports.Kernel = Kernel;
