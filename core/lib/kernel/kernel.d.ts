import { KernelConfiguration } from "../entity";
/**
 * @class Kernel
 * @description This class represent the core of the framework. It is the super-class of your application.
 * @author Thomas Gouveia
 */
declare class Kernel {
    /**
     * @var DEFAULT_PORT
     * @description the default port on which the app listen
     */
    private DEFAULT_PORT;
    /**
     * @var _app
     * @description The express application instance
     */
    private _app;
    /**
     * @var _modules
     * @description an array of all modules registered into the app
     */
    private _modules;
    /**
     * @var _middlewares
     * @description an array of all application middlewares registered into the app
     */
    private _middlewares;
    /**
     * @var _prefix
     * @description a prefix like "/api" to use in app
     */
    private _prefix?;
    /**
     * @var _gracefulShutdownHandler
     * @description function to execute when app receive signals SIGINT, SIGHUP or SIGTERM
     */
    private _gracefulShutdownHandler?;
    /**
     * @method constructor
     * @description Create an instance of the Kernel
     * @param config :
     */
    constructor(config: KernelConfiguration);
    /**
     * @method getPort()
     * @description return the port on which the application will listen
     * @returns the port
     */
    private _getPort;
    private _handleShutdownGracefully;
    /**
     * @method run()
     * @description run the application on the specified port, or by default on port 3000
     * @param port the port on which the application will listen
     */
    run(): void;
}
export { Kernel };
