declare class HealthService {
    checkForHealth(): string;
}
export default HealthService;
