import HealthService from "../services/health.service";
declare class HealthController {
    private healthService;
    constructor(healthService: HealthService);
    health(): {
        status: string;
    };
}
export default HealthController;
