"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var decorators_1 = require("../../../../decorators");
var health_service_1 = __importDefault(require("../services/health.service"));
var HealthController = /** @class */ (function () {
    function HealthController(healthService) {
        this.healthService = healthService;
    }
    HealthController.prototype.health = function () {
        var status = this.healthService.checkForHealth();
        return { status: status };
    };
    __decorate([
        decorators_1.Get("/"),
        decorators_1.Json(),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], HealthController.prototype, "health", null);
    HealthController = __decorate([
        decorators_1.Controller(),
        __metadata("design:paramtypes", [health_service_1.default])
    ], HealthController);
    return HealthController;
}());
exports.default = HealthController;
