import { Type } from "../../decorators/src/meta";
import { InjectionToken } from "./injection-token";
/**
 * Injectable type
 */
export declare type Injectable = Type | InjectionToken | string;
/**
 * Injectable id
 */
export declare type InjectableId = InjectionToken | string;
/**
 * DI Provider
 * @type Provider
 */
export declare type Provider = ClassProvider | FactoryProvider | ValueProvider;
/**
 * Class provider interface
 */
export interface ClassProvider {
    provide: Injectable;
    useClass: Type;
    deps?: Injectable[];
}
/**
 * Factory provider interface
 */
export interface FactoryProvider {
    provide: Injectable;
    useFactory: Factory;
    deps?: Injectable[];
}
/**
 * Value provider interface
 */
export interface ValueProvider {
    provide: Injectable;
    useValue: any;
}
/**
 * Store provider interface
 */
export interface StoreProvider {
    id: InjectableId;
    deps?: Dependency[];
    type?: Type;
    factory?: Factory;
    value?: any;
}
/**
 * Dependency interface
 */
export interface Dependency {
    id: InjectableId;
    optional?: boolean;
}
export declare type Factory = (...args: any[]) => any;
