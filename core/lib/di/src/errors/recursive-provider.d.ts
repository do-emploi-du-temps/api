import { StoreProvider } from "../types";
import { DependencyInjectionError } from "./error";
export declare class RecursiveProviderError extends DependencyInjectionError {
    constructor(requesters: StoreProvider[], depProvider: StoreProvider);
}
