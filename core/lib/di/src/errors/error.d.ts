import { Injectable, StoreProvider } from "../types";
export declare function id(injectable: StoreProvider | Injectable): string;
export declare class DependencyInjectionError extends Error {
    name: string;
    message: string;
    constructor(message: string);
}
