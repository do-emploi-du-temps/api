"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.RecursiveProviderError = void 0;
var error_1 = require("./error");
var RecursiveProviderError = /** @class */ (function (_super) {
    __extends(RecursiveProviderError, _super);
    function RecursiveProviderError(requesters, depProvider) {
        var _this = this;
        var circular = requesters
            .map(function (requester) { return requester.id.toString(); })
            .join(" => ");
        _this = _super.call(this, "\n      DI recursive dependency: " + circular + " => " + depProvider.id.toString() + ".\n    ") || this;
        return _this;
    }
    return RecursiveProviderError;
}(error_1.DependencyInjectionError));
exports.RecursiveProviderError = RecursiveProviderError;
