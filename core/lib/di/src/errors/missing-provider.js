"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.MissingProviderError = void 0;
var error_1 = require("./error");
var MissingProviderError = /** @class */ (function (_super) {
    __extends(MissingProviderError, _super);
    function MissingProviderError(provider, dep) {
        var _this = this;
        var msg = dep
            ? "\n        In order to get DI working, you have to provide Injectable.\n        DI attempt for " + error_1.id(provider) + " and dependency " + error_1.id(dep) + ".\n      "
            : "\n        In order to get DI working, you have to provide Injectable.\n        DI attempt for " + error_1.id(provider) + ".\n      ";
        _this = _super.call(this, msg) || this;
        return _this;
    }
    return MissingProviderError;
}(error_1.DependencyInjectionError));
exports.MissingProviderError = MissingProviderError;
