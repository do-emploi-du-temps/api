import { Dependency, Injectable, StoreProvider } from "../types";
import { DependencyInjectionError } from "./error";
export declare class MissingProviderError extends DependencyInjectionError {
    constructor(provider: StoreProvider | Injectable, dep?: Dependency);
}
