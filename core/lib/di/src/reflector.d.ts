import "reflect-metadata";
import { Type } from "../../decorators/src/meta";
import { InjectionToken } from "./injection-token";
import { Injectable } from "./types";
export declare class Reflector {
    /**
     * Get injectable id
     * @static
     * @param {Type} type
     *
     * @returns {InjectionToken}
     */
    static getId(type: Type): InjectionToken;
    /**
     * Set id for the type
     *
     * @static
     * @param {Type} type
     *
     * @returns {InjectionToken}
     */
    static setId(type: Type): InjectionToken;
    /**
     * Get injectable's param types
     *
     * @param {Injectable} target
     * @returns {Injectable[]}
     */
    static paramTypes(target: Type): Injectable[];
}
