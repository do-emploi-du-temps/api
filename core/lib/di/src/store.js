"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Store = void 0;
var injection_token_1 = require("./injection-token");
var reflector_1 = require("./reflector");
var Store = /** @class */ (function () {
    function Store() {
    }
    Store.providerId = function (injectable) {
        if (!injectable)
            return null;
        if (typeof injectable === "string" || injectable instanceof injection_token_1.InjectionToken)
            return injectable;
        return reflector_1.Reflector.getId(injectable);
    };
    Store.provider = function (type, args) {
        var _this = this;
        var provider = this.findProvider(type);
        if (provider === undefined) {
            provider = this.createProvider(type);
        }
        if (args === undefined) {
            reflector_1.Reflector.paramTypes(type).forEach(function (param, index) {
                if (!provider.deps[index] || !provider.deps[index].id) {
                    provider.deps[index] = { id: _this.providerId(param) };
                }
            });
            return type;
        }
        var dep = provider.deps[args.index] || { id: null };
        provider.deps[args.index] = {
            id: args.injectable ? this.providerId(args.injectable) : dep.id,
            optional: args.optional || dep.optional,
        };
        return type;
    };
    Store.findProvider = function (injectable) {
        var id = this.providerId(injectable);
        return this.providers.find(function (provider) { return provider.id === id; });
    };
    Store.replaceProvider = function (injectable, provider) {
        var storeProvider = this.findProvider(injectable);
        var index = this.providers.indexOf(storeProvider);
        if (index !== -1)
            this.providers[index] = provider;
        else
            this.providers.push(provider);
    };
    Store.createProvider = function (type) {
        var id = reflector_1.Reflector.setId(type);
        var storeProvider = { id: id, type: type, deps: [] };
        this.providers.push(storeProvider);
        return storeProvider;
    };
    Store.providers = [];
    return Store;
}());
exports.Store = Store;
