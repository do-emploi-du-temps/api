"use strict";
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Container = void 0;
var errors_1 = require("./errors");
var store_1 = require("./store");
var Container = /** @class */ (function () {
    function Container() {
    }
    Container.provide = function (providers) {
        var _this = this;
        providers
            .filter(function (_provider) { return _provider.useClass; })
            .forEach(function (_provider) { return _this.registerClassProvider(_provider); });
        providers
            .filter(function (_provider) { return _provider.useFactory; })
            .forEach(function (_provider) { return _this.registerFactoryProvider(_provider); });
        providers
            .filter(function (_provider) { return _provider.useValue; })
            .forEach(function (_provider) { return _this.registerValueProvider(_provider); });
    };
    Container.get = function (injectable) {
        var provider = store_1.Store.findProvider(injectable);
        if (provider === undefined) {
            throw new errors_1.MissingProviderError(injectable);
        }
        return this.resolveProvider(provider);
    };
    Container.resolveProvider = function (provider, requesters) {
        var _a;
        var _this = this;
        if (requesters === void 0) { requesters = []; }
        if (provider.value)
            return provider.value;
        var _requesters = requesters.concat([provider]);
        var deps = provider.deps.map(function (dep) {
            var requesterProvider = _requesters.find(function (requester) { return requester.id === dep.id; });
            if (requesterProvider)
                throw new errors_1.RecursiveProviderError(_requesters, requesterProvider);
            var depService = store_1.Store.findProvider(dep.id);
            if (!depService && !dep.optional) {
                throw new errors_1.MissingProviderError(provider, dep);
            }
            if (!depService && dep.optional) {
                return null;
            }
            return _this.resolveProvider(depService, _requesters);
        });
        provider.value = provider.factory ? provider.factory.apply(provider, deps) : new ((_a = provider.type).bind.apply(_a, __spreadArrays([void 0], deps)))();
        return provider.value;
    };
    Container.registerClassProvider = function (provider) {
        var id = store_1.Store.providerId(provider.provide);
        var classProvider = store_1.Store.findProvider(provider.useClass);
        var deps = classProvider
            ? classProvider.deps
            : (provider.deps || []).map(function (dep) { return ({
                id: store_1.Store.providerId(dep),
            }); });
        store_1.Store.replaceProvider(provider.provide, {
            id: id,
            deps: deps,
            type: provider.useClass,
        });
    };
    Container.registerFactoryProvider = function (provider) {
        var id = store_1.Store.providerId(provider.provide);
        var factory = provider.useFactory;
        var deps = (provider.deps || []).map(function (dep) { return ({
            id: store_1.Store.providerId(dep),
        }); });
        store_1.Store.replaceProvider(provider.provide, { id: id, factory: factory, deps: deps });
    };
    Container.registerValueProvider = function (provider) {
        var id = store_1.Store.providerId(provider.provide);
        var value = provider.useValue;
        store_1.Store.replaceProvider(provider.provide, { id: id, value: value });
    };
    return Container;
}());
exports.Container = Container;
