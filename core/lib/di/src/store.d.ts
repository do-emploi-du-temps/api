import { Type } from "../../decorators/src/meta";
import { Injectable, InjectableId, StoreProvider } from "./types";
export declare class Store {
    static providers: StoreProvider[];
    static providerId(injectable: Injectable): InjectableId;
    static provider(type: Type, args?: {
        injectable?: any;
        optional?: any;
        index?: any;
    }): Type;
    static findProvider(injectable: Injectable): StoreProvider;
    static replaceProvider(injectable: Injectable, provider: StoreProvider): void;
    private static createProvider;
}
