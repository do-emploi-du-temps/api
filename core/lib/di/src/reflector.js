"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Reflector = void 0;
require("reflect-metadata");
var injection_token_1 = require("./injection-token");
var Reflector = /** @class */ (function () {
    function Reflector() {
    }
    /**
     * Get injectable id
     * @static
     * @param {Type} type
     *
     * @returns {InjectionToken}
     */
    Reflector.getId = function (type) {
        return Reflect.getMetadata("__meta_di__", type);
    };
    /**
     * Set id for the type
     *
     * @static
     * @param {Type} type
     *
     * @returns {InjectionToken}
     */
    Reflector.setId = function (type) {
        var id = new injection_token_1.InjectionToken(type.name);
        Reflect.defineMetadata("__meta_di__", id, type);
        return id;
    };
    /**
     * Get injectable's param types
     *
     * @param {Injectable} target
     * @returns {Injectable[]}
     */
    Reflector.paramTypes = function (target) {
        return Reflect.getMetadata("design:paramtypes", target) || [];
    };
    return Reflector;
}());
exports.Reflector = Reflector;
