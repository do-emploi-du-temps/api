import { Injectable, Provider, StoreProvider } from "./types";
export declare class Container {
    static provide(providers: Provider[]): void;
    static get<T>(injectable: Injectable): T;
    static resolveProvider(provider: StoreProvider, requesters?: StoreProvider[]): any;
    private static registerClassProvider;
    private static registerFactoryProvider;
    private static registerValueProvider;
}
