/**
 * Injection token class
 */
export declare class InjectionToken {
    name: string;
    constructor(name: string);
    toString(): string;
}
