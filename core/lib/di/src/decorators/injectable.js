"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Injectable = void 0;
var store_1 = require("../store");
function Injectable() {
    return function (target) {
        return store_1.Store.provider(target);
    };
}
exports.Injectable = Injectable;
