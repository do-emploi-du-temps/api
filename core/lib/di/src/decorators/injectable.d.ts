import { Type } from "../../../decorators/src/meta";
export declare function Injectable(): (target: Type) => Type;
