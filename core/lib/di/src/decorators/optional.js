"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Optional = void 0;
var store_1 = require("../store");
function Optional() {
    return function (target, _key, index) {
        store_1.Store.provider(target, { index: index, optional: true });
    };
}
exports.Optional = Optional;
