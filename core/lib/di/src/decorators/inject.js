"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Inject = void 0;
var store_1 = require("../store");
function Inject(injectable) {
    return function (target, _propertyKey, index) {
        store_1.Store.provider(target, { index: index, injectable: injectable });
    };
}
exports.Inject = Inject;
