"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.InjectionToken = void 0;
/**
 * Injection token class
 */
var InjectionToken = /** @class */ (function () {
    function InjectionToken(name) {
        this.name = name;
    }
    InjectionToken.prototype.toString = function () {
        return this.name;
    };
    return InjectionToken;
}());
exports.InjectionToken = InjectionToken;
