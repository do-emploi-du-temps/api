import "colors";
declare class Logger {
    static info(...args: any[]): void;
    static error(...args: any[]): void;
    static success(...args: any[]): void;
    static debug(...args: any[]): void;
}
export { Logger };
