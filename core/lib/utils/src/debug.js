"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.debug = void 0;
var logger_1 = require("./logger");
function debug(modules, middlewares, prefix) {
    logger_1.Logger.debug("============================");
    logger_1.Logger.debug("Application configuration :");
    logger_1.Logger.debug("============================");
    if (prefix)
        logger_1.Logger.debug("Application prefix : " + prefix);
    for (var _i = 0, modules_1 = modules; _i < modules_1.length; _i++) {
        var module_1 = modules_1[_i];
        logger_1.Logger.debug(module_1.name.yellow + " attached.");
    }
}
exports.debug = debug;
