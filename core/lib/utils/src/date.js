"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.now = void 0;
var moment_1 = __importDefault(require("moment"));
var now = function () {
    return moment_1.default().format("DD/MM/YYYY HH:mm:ss");
};
exports.now = now;
