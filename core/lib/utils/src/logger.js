"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Logger = void 0;
require("colors");
var constants_1 = require("../../constants");
var date_1 = require("./date");
var Logger = /** @class */ (function () {
    function Logger() {
    }
    Logger.info = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        var typeColored = "[" + constants_1.LOG_LEVEL.INFO.blue + "]";
        for (var _a = 0, args_1 = args; _a < args_1.length; _a++) {
            var arg = args_1[_a];
            console.log(typeColored + " " + date_1.now() + " - " + arg);
        }
    };
    Logger.error = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        var typeColored = "[" + constants_1.LOG_LEVEL.ERROR.red + "]";
        for (var _a = 0, args_2 = args; _a < args_2.length; _a++) {
            var arg = args_2[_a];
            console.log(typeColored + " " + date_1.now() + " - " + arg);
        }
    };
    Logger.success = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        var typeColored = "[" + constants_1.LOG_LEVEL.SUCCESS.green + "]";
        for (var _a = 0, args_3 = args; _a < args_3.length; _a++) {
            var arg = args_3[_a];
            console.log("\n" + typeColored + " " + date_1.now() + " - " + arg);
        }
    };
    Logger.debug = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        var typeColored = "[" + constants_1.LOG_LEVEL.PROCESSING.blue + "]";
        for (var _a = 0, args_4 = args; _a < args_4.length; _a++) {
            var arg = args_4[_a];
            console.log(typeColored + " " + arg);
        }
    };
    return Logger;
}());
exports.Logger = Logger;
