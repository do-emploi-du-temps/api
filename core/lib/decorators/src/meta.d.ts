import { RouterOptions } from "express";
export interface Type extends Function {
    new (...args: any[]): any;
}
export declare enum ParameterType {
    REQUEST = 0,
    RESPONSE = 1,
    PARAMS = 2,
    QUERY = 3,
    BODY = 4,
    HEADERS = 5,
    COOKIES = 6,
    NEXT = 7
}
export interface ParameterConfiguration {
    index: number;
    type: ParameterType;
    name?: string;
    data?: any;
}
export interface Route {
    method: string;
    url: string;
    middleware: Type[];
    contentType?: string;
    statusCode?: number;
}
export interface ExpressMeta {
    url: string;
    routerOptions?: RouterOptions | null;
    middleware?: Type[];
    routes: {
        [key: string]: Route;
    };
    params: {
        [key: string]: ParameterConfiguration[];
    };
}
export interface ExpressDtoMeta {
    type: string;
    jsonScheme: {
        [key: number]: {
            jsonName: string;
            displayable: boolean;
        };
    };
}
export interface ExpressModuleMeta {
    path: string | RegExp;
    controllers: ExpressClass | null | any;
}
export interface ExpressDto {
    __express_dto_meta__?: ExpressDtoMeta;
}
export interface ExpressModule {
    __express_module_meta__?: ExpressModuleMeta;
}
export interface ExpressClass {
    __express_meta__?: ExpressMeta;
}
export declare function getClassMeta(target: ExpressClass): ExpressMeta;
export declare function getDtoMeta(target: ExpressDto): ExpressDtoMeta;
export declare function getModuleMeta(target: ExpressModule): ExpressModuleMeta;
