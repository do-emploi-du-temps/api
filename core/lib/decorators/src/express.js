"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.attachMiddlewares = exports.attachModules = void 0;
var express_1 = require("express");
var di_1 = require("../../di");
var decorators_1 = require("./decorators");
var meta_1 = require("./meta");
var middleware_1 = require("./middleware");
var jsonReplacer = function (key, value) {
    if (value instanceof Object && value.constructor.__express_dto_meta__) {
        var _a = value.constructor.__express_dto_meta__, type = _a.type, jsonScheme_1 = _a.jsonScheme;
        if (type === "dto") {
            var json_1 = {};
            Object.entries(value).forEach(function (_a, index) {
                var key = _a[0], propertyValue = _a[1];
                var meta = jsonScheme_1["" + index];
                if (meta) {
                    var jsonName = meta.jsonName, displayable = meta.displayable;
                    if (displayable)
                        json_1[jsonName] = propertyValue;
                }
                else
                    json_1[key] = propertyValue;
            });
            return __assign(__assign({}, json_1), { __meta__: {
                    __dto: true,
                    __class: value.constructor.name,
                } });
        }
    }
    return value;
};
function attachModules(app, modules, prefix) {
    modules.forEach(function (module) { return registerModule(app, module, prefix); });
    app.set("json replacer", jsonReplacer);
}
exports.attachModules = attachModules;
function attachMiddlewares(app, middleware) {
    middleware.forEach(function (mdw) { return app.use(mdw); });
}
exports.attachMiddlewares = attachMiddlewares;
function registerModule(app, module, prefix) {
    var _a = meta_1.getModuleMeta(module.prototype), path = _a.path, controllers = _a.controllers;
    controllers.forEach(function (controller) {
        var basePath;
        if (prefix && path === "/")
            basePath = prefix;
        else if (prefix && path !== "/")
            basePath = prefix + path;
        else
            basePath = path;
        registerController(app, basePath, controller, getController);
    });
    // Add error middleware handler
    app.use(middleware_1.errorMiddlewareHandler());
}
function registerController(app, basePath, Controller, _getController) {
    var _this = this;
    var controller = _getController(Controller);
    var meta = meta_1.getClassMeta(controller);
    var router = express_1.Router(meta.routerOptions);
    var routes = meta.routes, url = meta.url, params = meta.params;
    var controllerPath = basePath + (url || "");
    var routerMiddleware = (meta.middleware || []).map(function (middleware) {
        return middleware_1.middlewareHandler(middleware);
    });
    if (routerMiddleware.length) {
        router.use.apply(router, routerMiddleware);
    }
    var _loop_1 = function (methodName) {
        var route = routes[methodName];
        var controllerMethod = function (req, res, next) {
            var args = extractParameters(req, res, next, params[methodName]);
            var handler = controller[methodName].apply(controller, args);
            if (handler instanceof Promise) {
                handler.catch(next);
            }
            return handler;
        };
        var routeMiddleware = (route.middleware || []).map(function (middleware) {
            return middleware_1.middlewareHandler(middleware);
        });
        router[route.method].apply(router, __spreadArrays([
            route.url
        ], routeMiddleware, [
            function (req, res, next) { return __awaiter(_this, void 0, void 0, function () {
                var result, statusCode, err_1, httpCode, httpCodeText, message;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            _a.trys.push([0, 2, , 3]);
                            return [4 /*yield*/, controllerMethod(req, res, next)];
                        case 1:
                            result = _a.sent();
                            statusCode = route.statusCode;
                            if (decorators_1.ResponseContentTypes.JSON === route.contentType) {
                                res.status(statusCode || 200).json(result);
                            }
                            else {
                                res.status(statusCode || 200).send(result);
                            }
                            return [3 /*break*/, 3];
                        case 2:
                            err_1 = _a.sent();
                            httpCode = err_1.httpCode, httpCodeText = err_1.httpCodeText, message = err_1.message;
                            res.status(httpCode).json({
                                status: httpCode,
                                statusText: httpCodeText,
                                error: message,
                            });
                            return [3 /*break*/, 3];
                        case 3: return [2 /*return*/];
                    }
                });
            }); },
        ]));
    };
    for (var _i = 0, _a = Object.keys(routes); _i < _a.length; _i++) {
        var methodName = _a[_i];
        _loop_1(methodName);
    }
    ;
    app.use(controllerPath, router);
    return app;
}
function extractParameters(req, res, next, params) {
    if (!params || !params.length)
        return [req, res, next];
    var args = [];
    for (var _i = 0, params_1 = params; _i < params_1.length; _i++) {
        var _a = params_1[_i], name_1 = _a.name, index = _a.index, type = _a.type;
        switch (type) {
            case meta_1.ParameterType.RESPONSE:
                args[index] = res;
                break;
            case meta_1.ParameterType.REQUEST:
                args[index] = getParam(req, null, name_1);
                break;
            case meta_1.ParameterType.NEXT:
                args[index] = next;
                break;
            case meta_1.ParameterType.PARAMS:
                args[index] = getParam(req, "params", name_1);
                break;
            case meta_1.ParameterType.QUERY:
                args[index] = getParam(req, "query", name_1);
                break;
            case meta_1.ParameterType.BODY:
                args[index] = getParam(req, "body", name_1);
                break;
            case meta_1.ParameterType.HEADERS:
                args[index] = getParam(req, "headers", name_1);
                break;
            case meta_1.ParameterType.COOKIES:
                args[index] = getParam(req, "cookies", name_1);
                break;
        }
    }
    return args;
}
function getController(Controller) {
    try {
        return di_1.Container.get(Controller);
    }
    catch (_a) {
        return new Controller();
    }
}
function getParam(source, paramType, name) {
    var param = source[paramType] || source;
    return name ? param[name] : param;
}
