"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.errorMiddlewareHandler = exports.ERROR_MIDDLEWARE = exports.middlewareHandler = void 0;
var di_1 = require("../../di");
function middlewareHandler(middleware) {
    return function (req, res, next) {
        try {
            return getMiddleware(middleware, [req, res, next]);
        }
        catch (error) {
            next(error);
        }
    };
}
exports.middlewareHandler = middlewareHandler;
exports.ERROR_MIDDLEWARE = new di_1.InjectionToken("ERROR_MIDDLEWARE");
function errorMiddlewareHandler() {
    return function (error, req, res, next) {
        try {
            return getMiddleware(exports.ERROR_MIDDLEWARE, [error, req, res, next]);
        }
        catch (_a) {
            next(error);
        }
    };
}
exports.errorMiddlewareHandler = errorMiddlewareHandler;
function getMiddleware(middleware, args) {
    var next = args[args.length - 1];
    var instance;
    try {
        instance = di_1.Container.get(middleware);
    }
    catch (_a) {
        try {
            instance = new middleware();
        }
        catch (_b) {
            instance = middleware;
        }
    }
    var result = instance.use
        ? instance.use.apply(instance, args)
        : instance.apply(instance, args);
    if (result instanceof Promise) {
        result.catch(function (e) { return next(e); });
    }
    return result;
}
