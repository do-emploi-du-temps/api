"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.BadControllerReturnType = void 0;
var express_error_1 = require("./express-error");
var BadControllerReturnType = /** @class */ (function (_super) {
    __extends(BadControllerReturnType, _super);
    function BadControllerReturnType(controller, property) {
        return _super.call(this, "method " + property + "() in " + controller.name + " should return an HttpResponse.") || this;
    }
    return BadControllerReturnType;
}(express_error_1.ExpressFrameworkError));
exports.BadControllerReturnType = BadControllerReturnType;
