export declare class ExpressFrameworkError extends Error {
    name: string;
    message: string;
    constructor(message: string);
}
