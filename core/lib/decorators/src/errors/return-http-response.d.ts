import { Type } from "../meta";
import { ExpressFrameworkError } from "./express-error";
export declare class BadControllerReturnType extends ExpressFrameworkError {
    constructor(controller: Type | object, property: string);
}
