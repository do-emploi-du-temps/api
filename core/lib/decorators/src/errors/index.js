"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BadControllerReturnType = void 0;
var return_http_response_1 = require("./return-http-response");
Object.defineProperty(exports, "BadControllerReturnType", { enumerable: true, get: function () { return return_http_response_1.BadControllerReturnType; } });
