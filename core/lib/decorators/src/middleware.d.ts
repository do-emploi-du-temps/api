import { ErrorRequestHandler, NextFunction, Request, RequestHandler, Response } from "express";
import { InjectionToken } from "../../di";
import { Type } from "./meta";
export interface Middleware {
    use(request: Request, response: Response, next: NextFunction): void;
}
export interface ErrorMiddleware {
    use(error: any, request: Request, response: Response, next: NextFunction): void;
}
export declare function middlewareHandler(middleware: Type): RequestHandler;
export declare const ERROR_MIDDLEWARE: InjectionToken;
export declare function errorMiddlewareHandler(): ErrorRequestHandler;
