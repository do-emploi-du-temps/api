"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TextPlain = exports.Json = exports.ResponseContentTypes = void 0;
var meta_1 = require("../meta");
var ResponseContentTypes;
(function (ResponseContentTypes) {
    ResponseContentTypes["JSON"] = "json";
    ResponseContentTypes["TEXTPLAIN"] = "text";
})(ResponseContentTypes = exports.ResponseContentTypes || (exports.ResponseContentTypes = {}));
/**
 * Decorator factory to create decorators for routes
 *
 * @param {string} method
 * @param {string} url
 * @param {Type[]} middleware
 */
function decoratorFactory(contentType) {
    return function (target, key, descriptor) {
        var meta = meta_1.getClassMeta(target);
        meta.routes[key] = __assign(__assign({}, meta.routes[key]), { contentType: contentType });
        return descriptor;
    };
}
/**
 * Define a response content-type to JSON
 */
function Json() {
    return decoratorFactory(ResponseContentTypes.JSON);
}
exports.Json = Json;
/**
 * Define a response content-type to Text Plain
 */
function TextPlain() {
    return decoratorFactory(ResponseContentTypes.TEXTPLAIN);
}
exports.TextPlain = TextPlain;
