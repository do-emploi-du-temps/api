"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Module = void 0;
var meta_1 = require("../meta");
function Module(options) {
    return function (target) {
        var meta = meta_1.getModuleMeta(target.prototype);
        meta.path = options.path;
        meta.controllers = options.controllers;
    };
}
exports.Module = Module;
