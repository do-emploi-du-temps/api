/**
 * Decorates a route with the HTTP status code to return when the route is handled successfully.
 * @param {number} httpStatus Http status code of the request
 */
export declare function HttpCode(httpStatus: number): (target: any, key: string, descriptor: any) => any;
/**
 * Decorates a route to return HTTP 200 OK in case of success.
 * @see https://developer.mozilla.org/fr/docs/Web/HTTP/Status/200
 */
export declare function Ok(): (target: any, key: string, descriptor: any) => any;
/**
 * Decorates a route to return HTTP 201 Created in case of success.
 * @see https://developer.mozilla.org/fr/docs/Web/HTTP/Status/201
 */
export declare function Created(): (target: any, key: string, descriptor: any) => any;
/**
 * Decorates a route to return HTTP 202 Accepted in case of success.
 * @see https://developer.mozilla.org/fr/docs/Web/HTTP/Status/202
 */
export declare function Accepted(): (target: any, key: string, descriptor: any) => any;
/**
 * Decorates a route to return HTTP 204 No Content in case of success.
 * @see https://developer.mozilla.org/fr/docs/Web/HTTP/Status/204
 */
export declare function NoContent(): (target: any, key: string, descriptor: any) => any;
/**
 * Decorates a route to return HTTP 206 Partial Content in case of success.
 * @see https://developer.mozilla.org/fr/docs/Web/HTTP/Status/206
 */
export declare function PartialContent(): (target: any, key: string, descriptor: any) => any;
