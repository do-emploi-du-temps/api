import { ExpressClass } from "../meta";
export declare function Module(options: {
    path: string | RegExp;
    controllers: ExpressClass[] | any | null;
}): (target: any) => void;
