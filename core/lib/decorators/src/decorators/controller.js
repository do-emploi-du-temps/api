"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Controller = void 0;
var store_1 = require("../../../di/src/store");
var meta_1 = require("../meta");
/**
 * Decorate a class as a rest controller
 * @param {Object} options options to specify for the controller. "path" is the controller path
 */
function Controller(options) {
    return function (target) {
        if (options) {
            var meta = meta_1.getClassMeta(target.prototype);
            meta.url = options.path ? options.path : undefined;
            meta.middleware = Array.isArray(options.routerOptions)
                ? options.routerOptions
                : options.middlewares;
            meta.routerOptions = Array.isArray(options.routerOptions) ? null : options.routerOptions;
        }
        return store_1.Store.provider(target);
    };
}
exports.Controller = Controller;
