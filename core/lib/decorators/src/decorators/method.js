"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Head = exports.Options = exports.Put = exports.Patch = exports.Delete = exports.Post = exports.Get = void 0;
var meta_1 = require("../meta");
/**
 * Decorator factory to create decorators for routes
 *
 * @param {string} method
 * @param {string} url
 * @param {Type[]} middleware
 */
function decoratorFactory(method, url, middleware) {
    return function (target, key, descriptor) {
        var meta = meta_1.getClassMeta(target);
        meta.routes[key] = __assign(__assign({}, meta.routes[key]), { method: method, url: url, middleware: middleware });
        return descriptor;
    };
}
/**
 * Get route
 *
 * @param {string} url
 * @param {Type[]} [middleware]
 */
function Get(url, middleware) {
    var args = [];
    for (var _i = 2; _i < arguments.length; _i++) {
        args[_i - 2] = arguments[_i];
    }
    return decoratorFactory("get", url, middleware || []);
}
exports.Get = Get;
/**
 * Post route
 *
 * @param {string} url
 * @param {Type[]} [middleware]
 */
function Post(url, middleware) {
    return decoratorFactory("post", url, middleware || []);
}
exports.Post = Post;
/**
 * Delete route
 *
 * @param {string} url
 * @param {Type[]} [middleware]
 */
function Delete(url, middleware) {
    return decoratorFactory("delete", url, middleware || []);
}
exports.Delete = Delete;
/**
 * Patch route
 *
 * @param {string} url
 * @param {Type[]} [middleware]
 */
function Patch(url, middleware) {
    return decoratorFactory("patch", url, middleware || []);
}
exports.Patch = Patch;
/**
 * Put route
 *
 * @param {string} url
 * @param {Type[]} [middleware]
 */
function Put(url, middleware) {
    return decoratorFactory("put", url, middleware || []);
}
exports.Put = Put;
/**
 * Options route
 *
 * @param {string} url
 * @param {Type[]} [middleware]
 */
function Options(url, middleware) {
    return decoratorFactory("options", url, middleware || []);
}
exports.Options = Options;
/**
 * Head route
 *
 * @param {string} url
 * @param {Type[]} [middleware]
 */
function Head(url, middleware) {
    return decoratorFactory("head", url, middleware || []);
}
exports.Head = Head;
