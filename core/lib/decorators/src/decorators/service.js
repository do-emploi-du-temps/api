"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Service = void 0;
var store_1 = require("../../../di/src/store");
function Service() {
    return function (target) {
        return store_1.Store.provider(target);
    };
}
exports.Service = Service;
