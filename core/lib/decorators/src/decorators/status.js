"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PartialContent = exports.NoContent = exports.Accepted = exports.Created = exports.Ok = exports.HttpCode = void 0;
var meta_1 = require("../meta");
/**
 * Decorator factory to create decorators for status code
 *
 * @param {number} statusCode
 */
function decoratorFactory(statusCode) {
    return function (target, key, descriptor) {
        var meta = meta_1.getClassMeta(target);
        meta.routes[key] = __assign(__assign({}, meta.routes[key]), { statusCode: statusCode });
        return descriptor;
    };
}
/**
 * Decorates a route with the HTTP status code to return when the route is handled successfully.
 * @param {number} httpStatus Http status code of the request
 */
function HttpCode(httpStatus) {
    return decoratorFactory(httpStatus);
}
exports.HttpCode = HttpCode;
/**
 * Decorates a route to return HTTP 200 OK in case of success.
 * @see https://developer.mozilla.org/fr/docs/Web/HTTP/Status/200
 */
function Ok() {
    return decoratorFactory(200);
}
exports.Ok = Ok;
/**
 * Decorates a route to return HTTP 201 Created in case of success.
 * @see https://developer.mozilla.org/fr/docs/Web/HTTP/Status/201
 */
function Created() {
    return decoratorFactory(201);
}
exports.Created = Created;
/**
 * Decorates a route to return HTTP 202 Accepted in case of success.
 * @see https://developer.mozilla.org/fr/docs/Web/HTTP/Status/202
 */
function Accepted() {
    return decoratorFactory(202);
}
exports.Accepted = Accepted;
/**
 * Decorates a route to return HTTP 204 No Content in case of success.
 * @see https://developer.mozilla.org/fr/docs/Web/HTTP/Status/204
 */
function NoContent() {
    return decoratorFactory(204);
}
exports.NoContent = NoContent;
/**
 * Decorates a route to return HTTP 206 Partial Content in case of success.
 * @see https://developer.mozilla.org/fr/docs/Web/HTTP/Status/206
 */
function PartialContent() {
    return decoratorFactory(206);
}
exports.PartialContent = PartialContent;
