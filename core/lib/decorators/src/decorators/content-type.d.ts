export declare enum ResponseContentTypes {
    JSON = "json",
    TEXTPLAIN = "text"
}
/**
 * Define a response content-type to JSON
 */
export declare function Json(): (target: any, key: string, descriptor: any) => any;
/**
 * Define a response content-type to Text Plain
 */
export declare function TextPlain(): (target: any, key: string, descriptor: any) => any;
