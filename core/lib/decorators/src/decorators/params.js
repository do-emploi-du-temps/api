"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Cookies = exports.Headers = exports.Body = exports.Query = exports.Param = exports.Next = exports.Res = exports.Req = void 0;
var meta_1 = require("../meta");
function decoratorFactory(type) {
    return function (name) {
        return function (target, methodName, index) {
            var meta = meta_1.getClassMeta(target);
            if (meta.params[methodName] === undefined) {
                meta.params[methodName] = [];
            }
            meta.params[methodName].push({ index: index, type: type, name: name });
        };
    };
}
exports.Req = decoratorFactory(meta_1.ParameterType.REQUEST);
exports.Res = decoratorFactory(meta_1.ParameterType.RESPONSE);
exports.Next = decoratorFactory(meta_1.ParameterType.NEXT);
exports.Param = decoratorFactory(meta_1.ParameterType.PARAMS);
exports.Query = decoratorFactory(meta_1.ParameterType.QUERY);
exports.Body = decoratorFactory(meta_1.ParameterType.BODY);
exports.Headers = decoratorFactory(meta_1.ParameterType.HEADERS);
exports.Cookies = decoratorFactory(meta_1.ParameterType.COOKIES);
