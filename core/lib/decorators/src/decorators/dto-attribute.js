"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.JsonAttr = void 0;
var meta_1 = require("../meta");
function JsonAttr(jsonName, displayable) {
    if (displayable === void 0) { displayable = true; }
    return function (target, propertyKey, parameterIndex) {
        var meta = meta_1.getDtoMeta(target);
        meta.jsonScheme[parameterIndex] = { jsonName: jsonName, displayable: displayable };
    };
}
exports.JsonAttr = JsonAttr;
