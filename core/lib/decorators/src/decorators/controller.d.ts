import { RouterOptions } from "express";
import { Type } from "../meta";
/**
 * Decorate a class as a rest controller
 * @param {Object} options options to specify for the controller. "path" is the controller path
 */
export declare function Controller(options?: {
    path?: string;
    middlewares?: Type[];
    routerOptions?: Type[] | RouterOptions;
}): (target: any) => Type;
