export declare const Req: (name?: string) => ParameterDecorator;
export declare const Res: (name?: string) => ParameterDecorator;
export declare const Next: (name?: string) => ParameterDecorator;
export declare const Param: (name?: string) => ParameterDecorator;
export declare const Query: (name?: string) => ParameterDecorator;
export declare const Body: (name?: string) => ParameterDecorator;
export declare const Headers: (name?: string) => ParameterDecorator;
export declare const Cookies: (name?: string) => ParameterDecorator;
