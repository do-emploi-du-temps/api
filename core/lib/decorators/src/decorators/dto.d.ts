/**
 * Decorate a class as a rest controller
 * @param {Object} options options to specify for the controller. "path" is the controller path
 */
export declare function Dto(): (target: any) => void;
