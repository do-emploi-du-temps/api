export declare function JsonAttr(jsonName: string, displayable?: boolean): (target: any, propertyKey: string | symbol, parameterIndex: number) => void;
