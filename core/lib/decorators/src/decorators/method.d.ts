import { Type } from "../meta";
/**
 * Get route
 *
 * @param {string} url
 * @param {Type[]} [middleware]
 */
export declare function Get(url: string, middleware?: Type[], ...args: any[]): (target: any, key: string, descriptor: any) => any;
/**
 * Post route
 *
 * @param {string} url
 * @param {Type[]} [middleware]
 */
export declare function Post(url: string, middleware?: Type[]): (target: any, key: string, descriptor: any) => any;
/**
 * Delete route
 *
 * @param {string} url
 * @param {Type[]} [middleware]
 */
export declare function Delete(url: string, middleware?: Type[]): (target: any, key: string, descriptor: any) => any;
/**
 * Patch route
 *
 * @param {string} url
 * @param {Type[]} [middleware]
 */
export declare function Patch(url: string, middleware?: Type[]): (target: any, key: string, descriptor: any) => any;
/**
 * Put route
 *
 * @param {string} url
 * @param {Type[]} [middleware]
 */
export declare function Put(url: string, middleware?: Type[]): (target: any, key: string, descriptor: any) => any;
/**
 * Options route
 *
 * @param {string} url
 * @param {Type[]} [middleware]
 */
export declare function Options(url: string, middleware?: Type[]): (target: any, key: string, descriptor: any) => any;
/**
 * Head route
 *
 * @param {string} url
 * @param {Type[]} [middleware]
 */
export declare function Head(url: string, middleware?: Type[]): (target: any, key: string, descriptor: any) => any;
