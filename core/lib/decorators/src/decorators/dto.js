"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Dto = void 0;
var meta_1 = require("../meta");
/**
 * Decorate a class as a rest controller
 * @param {Object} options options to specify for the controller. "path" is the controller path
 */
function Dto() {
    return function (target) {
        var meta = meta_1.getDtoMeta(target.prototype);
        meta.type = "dto";
        meta.jsonScheme = {};
    };
}
exports.Dto = Dto;
