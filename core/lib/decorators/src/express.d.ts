import { Express, Router } from "express";
import { ExpressModule } from "./meta";
export declare function attachModules(app: Express | Router, modules: ExpressModule[], prefix?: string): void;
export declare function attachMiddlewares(app: Express | Router, middleware: any[]): void;
