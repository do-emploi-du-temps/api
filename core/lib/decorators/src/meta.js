"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getModuleMeta = exports.getDtoMeta = exports.getClassMeta = exports.ParameterType = void 0;
var ParameterType;
(function (ParameterType) {
    ParameterType[ParameterType["REQUEST"] = 0] = "REQUEST";
    ParameterType[ParameterType["RESPONSE"] = 1] = "RESPONSE";
    ParameterType[ParameterType["PARAMS"] = 2] = "PARAMS";
    ParameterType[ParameterType["QUERY"] = 3] = "QUERY";
    ParameterType[ParameterType["BODY"] = 4] = "BODY";
    ParameterType[ParameterType["HEADERS"] = 5] = "HEADERS";
    ParameterType[ParameterType["COOKIES"] = 6] = "COOKIES";
    ParameterType[ParameterType["NEXT"] = 7] = "NEXT";
})(ParameterType = exports.ParameterType || (exports.ParameterType = {}));
function getClassMeta(target) {
    if (!target.__express_meta__) {
        target.__express_meta__ = {
            url: "",
            middleware: [],
            routes: {},
            params: {},
        };
    }
    return target.__express_meta__;
}
exports.getClassMeta = getClassMeta;
function getDtoMeta(target) {
    if (!target.__express_dto_meta__) {
        target.__express_dto_meta__ = {
            type: "dto",
            jsonScheme: {},
        };
    }
    return target.__express_dto_meta__;
}
exports.getDtoMeta = getDtoMeta;
function getModuleMeta(target) {
    if (!target.__express_module_meta__) {
        target.__express_module_meta__ = {
            path: "",
            controllers: null,
        };
    }
    return target.__express_module_meta__;
}
exports.getModuleMeta = getModuleMeta;
