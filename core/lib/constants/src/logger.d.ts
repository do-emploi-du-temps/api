import "colors";
export declare enum LOG_LEVEL {
    INFO = "info",
    ERROR = "error",
    WARNING = "warning",
    SUCCESS = "success",
    PROCESSING = "debug"
}
