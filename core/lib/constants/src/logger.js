"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LOG_LEVEL = void 0;
require("colors");
var LOG_LEVEL;
(function (LOG_LEVEL) {
    LOG_LEVEL["INFO"] = "info";
    LOG_LEVEL["ERROR"] = "error";
    LOG_LEVEL["WARNING"] = "warning";
    LOG_LEVEL["SUCCESS"] = "success";
    LOG_LEVEL["PROCESSING"] = "debug";
})(LOG_LEVEL = exports.LOG_LEVEL || (exports.LOG_LEVEL = {}));
