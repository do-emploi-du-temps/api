"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SupportedMethods = void 0;
var SupportedMethods;
(function (SupportedMethods) {
    SupportedMethods["GET"] = "GET";
    SupportedMethods["POST"] = "POST";
    SupportedMethods["PUT"] = "PUT";
    SupportedMethods["DELETE"] = "DELETE";
})(SupportedMethods || (SupportedMethods = {}));
exports.SupportedMethods = SupportedMethods;
