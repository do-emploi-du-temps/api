"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ENV = exports.ENV_FILE = void 0;
var path_1 = require("path");
var Environments;
(function (Environments) {
    Environments["PROD"] = "prod";
    Environments["DEV"] = "dev";
})(Environments || (Environments = {}));
var ENV = process.env.NODE_ENV || Environments.DEV;
exports.ENV = ENV;
var ENV_FILE = path_1.resolve(__dirname, "..", "..", "..", "..", "environments", ".env." + ENV);
exports.ENV_FILE = ENV_FILE;
