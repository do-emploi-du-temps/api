FROM node:alpine
WORKDIR /app
COPY package.json ./package.json
RUN npm i
COPY src/ ./src
# to remove when framework is on npm
COPY core/lib ./core/lib
COPY core/package.json ./core/package.json
COPY ./AUTHORS.md ./AUTHORS.md
RUN cd core && npm i && cd ..
# end to remove
COPY tsconfig.json ./tsconfig.json
RUN npm run build && rm -r ./src && rm -r node_modules
RUN npm i --only=production
ENV TZ=Europe/Paris
ENTRYPOINT [ "npm", "start" ]
