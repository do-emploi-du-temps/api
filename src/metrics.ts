import client, { Registry } from "prom-client";

const registry: Registry = new client.Registry();

registry.setDefaultLabels({
  app: "do-edt-api",
});

client.collectDefaultMetrics({ register: registry });

const httpRequestDurationMicroseconds = new client.Histogram({
  name: "http_request_duration_seconds",
  help: "Duration of HTTP requests in microsecond",
  labelNames: ["method", "route", "code"],
  buckets: [0.1, 0.3, 0.5, 0.7, 1, 3, 5, 7, 10],
});

registry.registerMetric(httpRequestDurationMicroseconds);

export { registry, httpRequestDurationMicroseconds };
