import mongoose from "mongoose";

export const connectionFactory = () => {
  const connection = mongoose.createConnection(process.env.MONGODB_URI || "");
  // Create models here
  return connection;
};
