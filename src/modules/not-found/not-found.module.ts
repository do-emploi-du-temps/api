import { Module } from "typx";
import { NotFoundController } from "./controllers/not-found.controller";

@Module({
  path: "*",
  controllers: [NotFoundController],
})
export class NotFoundModule {}
