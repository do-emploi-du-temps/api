import { Controller, Get, NotFoundError, Req } from "typx";

@Controller()
export class NotFoundController {
  @Get("*")
  notFound(@Req("baseUrl") baseUrl: string, @Req("path") path: string) {
    throw new NotFoundError(`No handler found for route: ${baseUrl}${path}`);
  }
}
