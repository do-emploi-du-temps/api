import { Controller, Get, Json } from "typx";
import HttpResponseMiddleware from "../../metrics/middlewares/http-response-duration.middleware";
import WeekService from "../services/week.service";

@Controller()
class WeekController {
  constructor(private readonly _weekService: WeekService) {}

  @Get("/", [HttpResponseMiddleware])
  @Json()
  findAll() {
    return this._weekService.getAllWeeks();
  }
}

export default WeekController;
