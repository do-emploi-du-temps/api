import { Module } from "typx";
import WeekController from "./controllers/week.controller";

@Module({
  path: "/weeks",
  controllers: [WeekController],
})
export default class WeekModule {}
