import moment from "moment";
import { Service } from "typx";
import Week from "../dto/week.dto";

@Service()
class WeekService {
  /**
   * Get all weeks into the current year.
   * @returns {Week[]}
   */
  getAllWeeks(): Week[] {
    const year = moment().year();
    const totalWeeksInYear = Math.max(
      moment(new Date(year, 11, 31)).isoWeek(),
      moment(new Date(year, 11, 31 - 7)).isoWeek()
    );
    const weeks = [];
    for (let i = totalWeeksInYear; i > 0; i--) {
      const startOfWeek = moment().week(i).startOf("week").add(1, "day");
      const endOfWeek = startOfWeek.clone().add(4, "days");
      weeks.push(
        Week.fromJson({
          number: i,
          label:
            startOfWeek.format("D MMM") + " - " + endOfWeek.format("D MMM"),
        })
      );
    }
    return weeks.sort((a: Week, b: Week) => a.week - b.week);
  }
}

export default WeekService;
