import { Dto, JsonAttr } from "typx";

@Dto()
class Week {
  private constructor(
    @JsonAttr("number") private _weekNumber: number,
    @JsonAttr("label") private _weekLabel: string
  ) {}

  static fromJson(json: any): Week {
    return new Week(json.number, json.label);
  }

  get week(): number {
    return this._weekNumber;
  }
}

export default Week;
