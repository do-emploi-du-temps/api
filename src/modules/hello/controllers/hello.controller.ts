import { Controller, Get, Inject } from "typx";

@Controller()
export class HelloController {
  constructor(
    @Inject("meta.contributors")
    private readonly _contributors: string[] | string
  ) {}

  @Get("/")
  index() {
    return {
      message: "Hello from PolyEDT API !",
      version: "1.0.1",
      contributors: this._contributors,
    };
  }
}
