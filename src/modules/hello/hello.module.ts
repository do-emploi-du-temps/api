import { Module } from "typx";
import { HelloController } from "./controllers/hello.controller";

@Module({
  path: "/",
  controllers: [HelloController],
})
export class HelloModule {}
