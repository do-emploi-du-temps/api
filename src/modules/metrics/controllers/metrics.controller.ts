import { Response } from "express";
import { Registry } from "prom-client";
import { Controller, Get, Inject, Res } from "typx";

@Controller()
export class MetricsController {
  constructor(@Inject("core.metrics") private readonly registry: Registry) {}

  @Get("/")
  exposeMetrics(@Res() res: Response) {
    res.setHeader("Content-Type", this.registry.contentType);
    return this.registry.metrics();
  }
}
