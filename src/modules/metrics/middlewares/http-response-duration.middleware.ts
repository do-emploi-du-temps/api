import { NextFunction, Request, Response } from "express";
import { Histogram } from "prom-client";
import { Inject, Injectable, Middleware } from "typx";

@Injectable()
export default class HttpResponseMiddleware implements Middleware {
  constructor(
    @Inject("core.metrics.httpRequestsDuration")
    private readonly _metric: Histogram<any>
  ) {}

  public async use(req: Request, res: Response, next: NextFunction) {
    const timer = this._metric.startTimer();
    const baseSend = res.send;

    res.send = (data) => {
      res.send = baseSend;
      timer({
        route: req.baseUrl + req.path,
        code: res.statusCode,
        method: req.method,
      });
      return res.send(data);
    };
    next();
  }
}
