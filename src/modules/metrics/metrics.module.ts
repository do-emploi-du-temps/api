import { Module } from "typx";
import { MetricsController } from "./controllers/metrics.controller";

@Module({
  path: "/metrics",
  controllers: [MetricsController],
})
export class MetricsModule {}
