import { Service } from "typx";
import { id } from "typx/lib/di/src/errors/error";
import { Course } from "../../course/dto";
import { WeekState } from "../domain/domain";
import { Statistics } from "../dto/stats.dto";

@Service()
export class StatsService {
  /**
   *
   * @param week
   * @param courses
   */
  computeStats(week: number, courses: Map<number, Course[]>): Statistics {
    const totalMinutes = this._getTotalMinutesOfCourses(courses);
    return Statistics.fromJson({
      week,
      totalCourses: this._getTotalNumberOfCourses(courses),
      totalMinutes,
      state: this._getWeekLoad(totalMinutes / 60),
      bestIndex: this._getBestDay(courses),
      saddestIndex: this._getWorstDay(courses),
    });
  }

  /**
   * Return the load of the week
   * @param {number} hours
   * @returns {WeekState}
   */
  private _getWeekLoad(hours: number): WeekState {
    if (hours <= 25) return "LOW";
    else if (hours > 30) return "HIGH";
    else return "NORMAL";
  }

  /**
   * Compute the total number of courses in the week.
   * @param {Map<number, Course[]>} courses
   * @returns {number}
   */
  private _getTotalNumberOfCourses(courses: Map<number, Course[]>): number {
    let count = 0;
    courses.forEach((courseList: Course[]) => {
      count += courseList.length;
    });
    return count;
  }

  /**
   * Compute the total minutes of courses in the week.
   * @param {Map<number, Course[]>} courses
   * @returns {number}
   */
  private _getTotalMinutesOfCourses(courses: Map<number, Course[]>): number {
    let count = 0;
    courses.forEach((courseList: Course[]) => {
      courseList.forEach((course: Course) => {
        count += course.duration;
      });
    });
    return count;
  }

  /**
   * Find the best day in the week
   */
  private _getBestDay(week: Map<number, Course[]>): number {
    let idx = 0;
    let previousDuration = 10000;
    week.forEach((courses: Course[], dayIdx: number) => {
      const totalDuration = courses
        .map((c) => c.duration)
        .reduce((accumulator, current) => accumulator + current);
      if (totalDuration < previousDuration) {
        previousDuration = totalDuration;
        idx = dayIdx;
      }
    });
    return idx;
  }

  /**
   * Find the saddest day in the week
   */
  private _getWorstDay(week: Map<number, Course[]>): number {
    let idx = 0;
    let previousDuration = 0;
    week.forEach((courses: Course[], dayIdx: number) => {
      const totalDuration = courses
        .map((c) => c.duration)
        .reduce((accumulator, current) => accumulator + current);
      if (totalDuration > previousDuration) {
        previousDuration = totalDuration;
        idx = dayIdx;
      }
    });
    return idx;
  }
}
