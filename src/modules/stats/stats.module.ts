import { Module } from "typx";
import { StatsController } from "./controllers/stats.controller";

@Module({
  path: "/stats",
  controllers: [StatsController],
})
export class StatsModule {}
