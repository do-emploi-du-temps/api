import { Dto, JsonAttr } from "typx";
import { StatisticsSchema, WeekState } from "../domain/domain";

@Dto()
export class Statistics {
  private constructor(
    @JsonAttr("week")
    private readonly _week: number,
    @JsonAttr("totalCourses")
    private readonly _totalCoursesInWeek: number,
    @JsonAttr("totalMinutes")
    private readonly _totalCoursesDurationInWeek: number,
    @JsonAttr("load")
    private readonly _state: WeekState,
    @JsonAttr("bestDay")
    private readonly _bestIndex: number,
    @JsonAttr("worstDay")
    private readonly _saddestIndex: number
  ) {}

  static fromJson(json: StatisticsSchema): Statistics {
    return new Statistics(
      json.week,
      json.totalCourses,
      json.totalMinutes,
      json.state,
      json.bestIndex,
      json.saddestIndex
    );
  }
}
