import {
  Controller,
  Get,
  HttpError,
  InternalServerError,
  NotFoundError,
  Param,
} from "typx";
import { Course } from "../../course/dto";
import { CourseService } from "../../course/services";
import HttpResponseMiddleware from "../../metrics/middlewares/http-response-duration.middleware";
import { StatsService } from "../services/stats.service";

@Controller()
export class StatsController {
  constructor(
    private readonly _courseService: CourseService,
    private readonly _statsService: StatsService
  ) {}

  @Get("/:promo/:week", [HttpResponseMiddleware])
  async getStats(
    @Param("promo") promo: string,
    @Param("week") weekString: string
  ) {
    const week = parseInt(weekString, 10);
    if (week < 0 || isNaN(week))
      throw new InternalServerError(`Week : ${weekString} is invalid.`);
    const courses:
      | Map<number, Course[]>
      | HttpError
      | undefined = await this._courseService.getCoursesByWeek(promo, week);
    if (courses instanceof HttpError) throw courses;
    if (!courses) throw new NotFoundError(`No stats for week ${week}.`);

    return this._statsService.computeStats(week, courses);
  }
}
