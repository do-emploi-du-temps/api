export interface StatisticsSchema {
  week: number;
  totalCourses: number;
  totalMinutes: number;
  state: WeekState;
  bestIndex: number;
  saddestIndex: number;
}

export type WeekState = "LOW" | "NORMAL" | "HIGH";
