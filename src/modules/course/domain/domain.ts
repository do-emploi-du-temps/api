export interface CourseColor {
  bgColor: string;
  txtColor: string;
}

export interface ColorData {
  [key: string]: CourseColor;
}

export interface RGB {
  r: number;
  g: number;
  b: number;
}

export enum BaseColors {
  BLACK = "#000000",
  WHITE = "#FFFFFF",
}
