import moment, { Moment } from "moment";
import { Dto, JsonAttr } from "typx";
import { CourseColor } from "../domain/domain";
import { IcalEvent } from "./event.dto";

@Dto()
export class Course {
  private constructor(
    @JsonAttr("uid") private _uid: string,
    @JsonAttr("title") private _title: string,
    @JsonAttr("room") private _room: string,
    @JsonAttr("start") private _start: string,
    @JsonAttr("end") private _end: string,
    @JsonAttr("indexInWeek") private _indexInWeek: number,
    @JsonAttr("bgColor") private _backgroundColor: string | null,
    @JsonAttr("txtColor") private _textColor: string | null,
    @JsonAttr("tutor") private _tutor: string | null,
    @JsonAttr("week") private _week: number,
    @JsonAttr("minutesFromMidnight") private _fromMidnight: number,
    @JsonAttr("duration") private _duration: number
  ) {}

  static merge(courses: Course[]): Course[] {
    const result: Course[] = [];
    const mergeMap: { [key: string]: Course[] } = {};
    // Init merge map
    for (const course of courses) {
      const { title } = course;
      const mergeItem: Course[] = mergeMap[title] || [];
      mergeItem.push(course);
      mergeMap[title] = mergeItem;
    }

    // Merge items
    for (const [_, v] of Object.entries(mergeMap)) {
      let course: Course | null = null;
      v.forEach((_course: Course, index: number) => {
        if (index === 0 || !course) {
          course = _course;
          return;
        }
        course.addDuration(_course.duration);
        course.end = _course.end;
      });
      if (course) result.push(course);
    }
    return result;
  }

  static fromIcalEvent(event: IcalEvent): Course {
    return new Course(
      event.id,
      event.title,
      event.room,
      event.startMoment.toISOString(true),
      event.endMoment.toISOString(true),
      event.indexInWeek,
      null,
      null,
      event.tutor,
      event.week,
      event.minutesFromMidnight,
      event.duration
    );
  }

  public get uid(): string {
    return `${this._duration}_${this._indexInWeek}_${this._duration}_${this._fromMidnight}`;
  }

  public get title(): string {
    return this._title;
  }

  public set colors(colors: CourseColor) {
    this._backgroundColor = colors.bgColor;
    this._textColor = colors.txtColor;
  }

  public get week(): number {
    return this._week;
  }

  public get day(): number {
    return this._indexInWeek;
  }

  public get start(): Moment {
    return moment(this._start);
  }

  public get end(): Moment {
    return moment(this._end);
  }

  public set end(end: Moment) {
    this._end = end.toISOString(true);
  }

  public get duration(): number {
    return this._duration;
  }

  public addDuration(duration: number) {
    this._duration += duration;
  }

  public isNextTo(previousCourse: Course): boolean {
    return this.start.diff(previousCourse.end, "minutes") === 0;
  }
}
