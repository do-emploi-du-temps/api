import moment, { Moment } from "moment";

export class IcalEvent {
  private constructor(
    private type: string,
    private params: any[],
    private dtstramp: string,
    private start: string,
    private datetype: string,
    private end: string,
    private summary: string,
    private location: string,
    private description: string,
    private uid: string,
    private created: string,
    private lastModified: string,
    private sequence: string
  ) {}

  static fromJson(json: any): IcalEvent {
    return new IcalEvent(
      json.type,
      json.params,
      json.dtstamp,
      json.start,
      json.datetype,
      json.end,
      json.summary,
      json.location,
      json.description,
      json.uid,
      json.created,
      json.lastModified,
      json.sequence
    );
  }

  public get title(): string {
    return this.summary;
  }

  public get desc(): string {
    return this.description;
  }

  public get startMoment(): Moment {
    return moment(this.start);
  }

  public get endMoment(): Moment {
    return moment(this.end);
  }

  public get duration(): number {
    return this.endMoment.diff(this.startMoment, "minutes");
  }

  public get room(): string {
    return this.location;
  }

  public get id(): string {
    return this.uid;
  }

  public get tutor(): string | null {
    const tutor = this.desc
      .replace(/\(.+\)|[a-zA-Z0-9-]*\d[a-zA-Z0-9-]*/g, "")
      .replace("A valider", "")
      .trim()
      .split("\n")[0]
      .replace("   ", " ")
      .split(" ");
    const name = tutor.reverse().join(" ");
    return name === "" || !name ? null : name;
  }

  public get indexInWeek(): number {
    return this.startMoment.weekday();
  }

  public get minutesFromMidnight(): number {
    return this.startMoment.diff(this._midnight, "minutes");
  }

  private get _midnight(): Moment {
    return this.startMoment.clone().startOf("day");
  }

  public get week(): number {
    return this.startMoment.week();
  }

  public isEnded(): boolean {
    const diff = moment().diff(this.endMoment, "months");
    return diff > 1;
  }
}
