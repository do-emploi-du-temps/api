import { Inject, InternalServerError, Service } from "typx";
import { colorsFileToken } from "../../../providers";
import { ColorData } from "../domain/domain";
import { Course } from "../dto";
import FileService from "./file.service";
import RgbService from "./rgb.service";

@Service()
export class ColorService {
  constructor(
    private readonly _fileService: FileService,
    private readonly _rgbService: RgbService,
    @Inject(colorsFileToken) private readonly _file: string
  ) {}

  /**
   * Retrieves the colors of an IcalEvent
   * @param {Course} course the course
   * @returns {}
   */
  async addColorsToCourses(
    courses: Course[]
  ): Promise<Course[] | InternalServerError> {
    let colors: ColorData = await this._getColorsData();
    const _courses = courses.map((course: Course) => {
      const hasColor = colors[course.title] !== undefined;
      const _colors = hasColor
        ? colors[course.title]
        : this._rgbService.generateColors();
      if (!hasColor) colors[course.title] = _colors;
      course.colors = _colors;
      return course;
    });
    const hasSaved = this._saveColors(colors);
    return hasSaved
      ? _courses
      : new InternalServerError(
          `Unable to save colors in file : ${this._file}. Please see the application logs to more details.`
        );
  }

  /**
   * Get the colors data from data file.
   * @returns {ColorData}
   */
  private async _getColorsData(): Promise<ColorData> {
    const data = await this._fileService.readFile(this._file);
    return data ? JSON.parse(data) : {};
  }

  /**
   * Save colors to the data file.
   * @param {ColorData} colors
   * @returns {boolean}
   */
  private async _saveColors(colors: ColorData): Promise<boolean> {
    return await this._fileService.writeFile(
      this._file,
      JSON.stringify(colors)
    );
  }
}
