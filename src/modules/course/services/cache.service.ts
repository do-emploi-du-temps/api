import NodeCache from "node-cache";
import { Inject, Service } from "typx";
import { cacheToken } from "../../../providers";

@Service()
export class CacheService {
  constructor(@Inject(cacheToken) private cache: NodeCache) {}

  /**
   * Put a key / value couple in the cache.
   * @param {string} key
   * @param {any} value
   * @returns {T}
   */
  set<T>(key: string, value: T): T {
    this.cache.set(key, value);
    return value;
  }

  /**
   * Check if the key is present in cache
   * @param {string} key
   * @returns {boolean}
   */
  has(key: string): boolean {
    return this.cache.has(key);
  }

  /**
   * Retrive a value with the key provided in paramaters of undefined if there is no value.
   * @param {string} key
   * @returns {T}
   */
  get<T>(key: string): T {
    return this.cache.get(key) as T;
  }
}
