import randomColor from "randomcolor";
import { Service } from "typx";
import { BaseColors, CourseColor, RGB } from "../domain/domain";

@Service()
class RgbService {
  generateColors(): CourseColor {
    const color = randomColor();
    return { bgColor: color, txtColor: this._contrast(color) };
  }

  private _rgbToYIQ({ r, g, b }: RGB): number {
    return (r * 299 + g * 587 + b * 114) / 1000;
  }

  private _hexToRgb(hex: string): RGB | undefined {
    if (!hex || hex === undefined || hex === "") return undefined;
    const result: RegExpExecArray | null = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(
      hex
    );
    return result
      ? {
          r: parseInt(result[1], 16),
          g: parseInt(result[2], 16),
          b: parseInt(result[3], 16),
        }
      : undefined;
  }

  private _contrast(
    colorHex: string | undefined,
    threshold: number = 128
  ): string {
    if (colorHex === undefined) return BaseColors.BLACK;
    const rgb: RGB | undefined = this._hexToRgb(colorHex);
    if (rgb === undefined) return BaseColors.BLACK;
    return this._rgbToYIQ(rgb) >= threshold
      ? BaseColors.BLACK
      : BaseColors.WHITE;
  }
}

export default RgbService;
