import ical from "node-ical";
import { InternalServerError, Service } from "typx";
import { IcalEvent } from "../dto";
import axios from "axios";
import { ApplicationError } from "../../../constants/errors";

@Service()
export class IcalService {
  private readonly _base: string =
    "https://proseconsult.umontpellier.fr/jsp/custom/modules/plannings/direct_cal.jsp?data=";
  /**
   * Fetch events from the ICAL link and returns a list of IcalEvent.
   * @returns {IcalEvent[] | InternalServerError}
   */
  async getEvents(icalUrl: string): Promise<IcalEvent[] | InternalServerError> {
    const toFetch = `${this._base}${icalUrl}`;
    try {
      // Parse the ical content
      const { data } = await axios.get(toFetch, { timeout: 5000 });
      // If HTML
      if (data.startsWith("<!DOCTYPE"))
        throw ApplicationError.PROSE_MAINTENANCE;
      const result = await ical.async.fromURL(toFetch);
      // Map each event to an IcalEvent object, and filter on available events
      return Object.values(result)
        .map(IcalEvent.fromJson)
        .filter((icalEvent: IcalEvent) => !icalEvent.isEnded());
    } catch (err) {
      return new InternalServerError(
        err === ApplicationError.PROSE_MAINTENANCE
          ? err
          : `Unable to parse the URL : ${toFetch}.`
      );
    }
  }
}
