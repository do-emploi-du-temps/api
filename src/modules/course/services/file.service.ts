import fs from "fs/promises";
import { Service } from "typx";

@Service()
class FileService {
  /**
   * Read the file. Return null if an error has occured.
   * @param {string} path
   * @returns {string | null}
   */
  async readFile(path: string): Promise<string | null> {
    try {
      const buffer = await fs.readFile(path);
      return buffer.toString();
    } catch (err) {
      return null;
    }
  }

  /**
   * Write content into a file. Return false if an error occured, true otherwise.
   * @param {string} path
   * @param {string} content
   * @returns {boolean}
   */
  async writeFile(path: string, content: string): Promise<boolean> {
    try {
      await fs.writeFile(path, content);
      return true;
    } catch (err) {
      console.error(err);
      return false;
    }
  }
}

export default FileService;
