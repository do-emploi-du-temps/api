import { Inject, NotFoundError, Service } from "typx";
import { HttpError } from "typx/lib/errors/src/http/error";
import { promosToken } from "../../../providers";
import { Course, IcalEvent } from "../dto";
import { CacheService } from "./cache.service";
import { ColorService } from "./color.service";
import { IcalService } from "./ical.service";

@Service()
export class CourseService {
  /**
   * The key used to store courses into the cache.
   */
  private CACHE_KEY: string = "courses";

  constructor(
    private readonly _icalService: IcalService,
    private readonly _cacheService: CacheService,
    private readonly _colorService: ColorService,
    @Inject(promosToken) private _promos: Map<string, string>
  ) {}

  /**
   * Fetch the events from the ICAL and create a courses list.
   * @returns {Map<number, Course[]> | Error}
   */
  async getCourses(
    promo: string,
    reload: boolean
  ): Promise<Map<number, Map<number, Course[]>> | HttpError> {
    const computedCacheKey = `${this.CACHE_KEY}_${promo}`;
    // We check if there is courses in cache
    if (this._cacheService.has(computedCacheKey) && !reload) {
      // If we have something in cache, we return for best performance
      return this._cacheService.get<Map<number, Map<number, Course[]>>>(
        computedCacheKey
      );
    }
    // Get ICAL events
    const result: IcalEvent[] | HttpError = await this._icalService.getEvents(
      this._promos.get(promo) || ""
    );
    // If the result if and error, we return immediatly
    if (result instanceof HttpError) return result;
    const coursewWithColors = await this._colorService.addColorsToCourses(
      result.map(Course.fromIcalEvent)
    );
    if (coursewWithColors instanceof HttpError) return coursewWithColors;
    // Build the course list from the ical events
    const courses: Map<number, Map<number, Course[]>> = this._mapCoursesByWeek(
      coursewWithColors
    );
    // We cache the result
    return this._cacheService.set<Map<number, Map<number, Course[]>>>(
      computedCacheKey,
      courses
    );
  }

  /**
   * Build a course map with week numbers in entries and courses list in value.
   * @param {Course[]} courses the list of courses to arrange
   * @returns {Map<number, Map<number, Course[]>>}
   */
  private _mapCoursesByWeek(
    courses: Course[]
  ): Map<number, Map<number, Course[]>> {
    const map = new Map<number, Map<number, Course[]>>();
    // For each courses
    courses.forEach((course: Course) => {
      // We get the week and the day of the course
      const { week, day } = course;

      // Trying to get the weekMap from the map.
      // If we don't have any map for this week, create it
      const weekMap: Map<number, Course[]> =
        map.get(week) || new Map<number, Course[]>();

      // Trying to get the day courses list from the week map.
      // If it doesn't exist, create an empty list
      const dayList: Course[] = weekMap.get(day) || [];
      dayList.push(course);

      // Save
      weekMap.set(
        day,
        dayList.sort((a, b) => (b.start.isAfter(a.start) ? -1 : 1))
      );
      map.set(week, this._checkForRedundant(weekMap));
    });
    return map;
  }

  /**
   * Get courses by week number
   * @param {number} week
   * @returns {Course[] | undefined | HttpError}
   */
  async getCoursesByWeek(
    promo: string,
    week: number,
    reload?: boolean
  ): Promise<Map<number, Course[]> | undefined | HttpError> {
    if (!this._promos.has(promo)) {
      return new NotFoundError(`Promo : ${promo} not found.`);
    }
    const courses = await this.getCourses(promo, reload ?? false);
    if (courses instanceof HttpError) return courses;
    return courses.get(week);
  }

  /**
   * Find redundants courses (eg courses chaining) and return indexes in list.
   * @param {Course[]} courses
   */
  private _getRedundants(courses: Course[]): { [index: number]: Course } {
    let tmp: { [index: number]: Course } = {};
    for (let i = 0; i < courses.length; i++) {
      let cursor: Course = courses[i];
      for (let y = i + 1; y < courses.length; y++) {
        let currentCourse = courses[y];
        if (currentCourse.isNextTo(cursor)) {
          if (!tmp[i]) tmp[i] = cursor;
          tmp[y] = currentCourse;
        }
      }
    }
    return tmp;
  }

  /**
   * Check if there is redundants courses in week list.
   * @param {Map<number, Course[]>} courses
   * @returns {Map<number, Course[]>}
   */
  private _checkForRedundant(
    courses: Map<number, Course[]>
  ): Map<number, Course[]> {
    courses.forEach((courseList: Course[], dayIndex: number) => {
      const redundants = this._getRedundants(courseList);
      const filtered = courseList.filter((_, index) => !redundants[index]);
      courses.set(dayIndex, [
        ...filtered,
        ...Course.merge(Object.values(redundants)),
      ]);
    });
    return courses;
  }
}
