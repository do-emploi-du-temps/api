import {
  Controller,
  Get,
  HttpError,
  InternalServerError,
  Json,
  NotFoundError,
  Param,
  Query,
} from "typx";
import HttpResponseMiddleware from "../../metrics/middlewares/http-response-duration.middleware";
import { Course } from "../dto";
import { CourseService } from "../services";
@Controller()
export default class CourseController {
  constructor(private readonly _courseService: CourseService) {}

  @Get("/:promo/:week", [HttpResponseMiddleware])
  @Json()
  async findForWeek(
    @Param("promo") promo: string,
    @Param("week") weekString: string,
    @Query("reload") reload?: string
  ) {
    const week = parseInt(weekString, 10);
    if (week < 0 || isNaN(week))
      throw new InternalServerError(`Week : ${weekString} is invalid.`);
    // Get courses for week
    const result:
      | Map<number, Course[]>
      | HttpError
      | undefined = await this._courseService.getCoursesByWeek(
      promo,
      week,
      Boolean(reload)
    );
    if (result instanceof HttpError) throw result;
    if (!result) throw new NotFoundError(`No data found for week ${week}.`);
    return { promo, week, courses: Object.fromEntries(result) };
  }
}
