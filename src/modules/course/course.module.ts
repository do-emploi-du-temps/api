import { Module } from "typx";
import CourseController from "./controllers/course.controller";

@Module({
  path: "/courses",
  controllers: [CourseController],
})
export default class CourseModule {}
