export enum ApplicationError {
  PROSE_MAINTENANCE = "Prose service under maintenance.",
}
