/**
 * Map to store promos & ical data id
 * @var promos
 */
const promos = new Map<string, string>(
  Object.entries({
    DO3:
      "58c99062bab31d256bee14356aca3f2423c0f022cb9660eba051b2653be722c431b66c493702208e664667048bc04373dc5c094f7d1a811b903031bde802c7f59b21846d3c6254443d7b6e956d3145c6e0d5bac87b70fdd185b8b86771d71211cc671c69a486a302536110eaa3e6c593,1",
  })
);

export default promos;
