import { readFileSync } from "fs";
import NodeCache from "node-cache";
import { resolve } from "path";
import { Container, InjectionToken } from "typx";
import { connectionFactory } from "./bootstrap/database";
import promos from "./constants/promo";
import { httpRequestDurationMicroseconds, registry } from "./metrics";

// Env value or 6hours
const cacheTtl = process.env.CACHE_TTL
  ? parseInt(process.env.CACHE_TTL)
  : 21600;
const cacheToken = new InjectionToken("cache");
const icalUrlToken = new InjectionToken("icalUrl");
const colorsFileToken = new InjectionToken("colorsFile");
const promosToken = new InjectionToken("promosToken");

Container.provide([
  {
    provide: promosToken,
    useValue: promos,
  },
  {
    provide: cacheToken,
    useValue: new NodeCache({
      stdTTL: cacheTtl,
      checkperiod: cacheTtl * 0.2,
      useClones: false,
    }),
  },
  {
    provide: colorsFileToken,
    useValue: resolve(__dirname, "..", "colors.json"),
  },
  {
    provide: "core.metrics",
    useValue: registry,
  },
  {
    provide: "core.metrics.httpRequestsDuration",
    useValue: httpRequestDurationMicroseconds,
  },
  {
    provide: "meta.contributors",
    useFactory: () => {
      const authorsFile: string = resolve(__dirname, "..", "AUTHORS.md");
      try {
        const contributors = readFileSync(authorsFile).toString();
        return contributors.trim().split("\n");
      } catch {
        return `[ERROR] Failed to fetch contributors from ${authorsFile}`;
      }
    },
  },
  {
    provide: "mongodb.connection",
    useFactory: connectionFactory,
  },
]);

export { cacheToken, promosToken, colorsFileToken };
