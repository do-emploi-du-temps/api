import cors from "cors";
import morgan from "morgan";
import { Kernel } from "typx";
import CourseModule from "./modules/course/course.module";
import { HelloModule } from "./modules/hello/hello.module";
import { MetricsModule } from "./modules/metrics/metrics.module";
import { NotFoundModule } from "./modules/not-found/not-found.module";
import { StatsModule } from "./modules/stats/stats.module";
import WeekModule from "./modules/weeks/week.module";

// Export your application
// you can add some functionalities here like database connection for example
export default class extends Kernel {
  constructor() {
    super({
      modules: [
        CourseModule,
        WeekModule,
        MetricsModule,
        StatsModule,
        HelloModule,
        NotFoundModule,
      ],
      middlewares: [
        cors(),
        morgan(process.env.NODE_ENV != "prod" ? "dev" : "combined"),
      ],
      healthModule: true,
      onGracefulShutdown: (server) => {
        server.close(() => {
          console.log("Application exited.");
          process.exit(0);
        });
      },
    });
  }
}
